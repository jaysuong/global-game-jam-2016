# README #

This repo is for the Global Game Jam 2016 at Microsoft in NYC.  The theme of the jam is **ritual**, and our response is a game called **All Hail the Cloud** where the cultist must appease the cloud god by building monuments.

This game is made with Unity.  Target platforms are IOS, Android, and Windows Phone.

[More Info Here](http://globalgamejam.org/2016/games/all-hail-cloud-0)

### Who made this game ###

* **Jay Suong** for coding the core system and game mechanics
* **Nannan Yao** for coding mobile input controllers, throwing mechanics, and game design
* **Chris Zito** for artwork and game design
* **Rob Zahn** for sound effects and music
* **Kejana Williams** for game design and jitter code

### How do I get set up? ###

Clone and open with Unity 5.3 or higher.