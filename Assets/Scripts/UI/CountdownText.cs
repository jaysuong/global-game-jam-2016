using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountdownText : MonoBehaviour {

    public string[] texts;
    public float interval = 1f;

    public GameObject hud;

    public Text text;

    int index;

    GameManager manager;

    void Start() {
        manager = FindObjectOfType<GameManager>();
        StartCoroutine(Countdown());
    }

    IEnumerator Countdown() {
        while(index < texts.Length) {
            text.text = texts[index++];
            yield return new WaitForSeconds(interval);
        }
        manager.RunStartables();
        hud.SetActive(true);
        gameObject.SetActive(false);
    }


}
