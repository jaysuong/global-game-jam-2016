using UnityEngine;
using System.Collections;

public class ReloadGame : MonoBehaviour {

    public string scene = "Main Game";

	public void Reload() {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
