using UnityEngine;
using System.Collections;

public enum TimeState { NotStarted, Running, Stopped }

/// <summary>
/// Delegate used for the event when time runs out
/// </summary>
public delegate void OutOfTimeHandler();


public class TimeManager : MonoBehaviour, IStartable {

    public event OutOfTimeHandler OutOfTimeEvent;

    [Tooltip("How long should the game last")]
    public float gameLength = 90;

    public TimeState state { get; private set; }

    /// <summary>
    /// How much time is left
    /// </summary>
    public float timeLeft { get; private set; }

    public float elaspedTime { get { return gameLength - timeLeft; } }


    void Awake() {
        state = TimeState.NotStarted;
        timeLeft = gameLength;
    }

    public void Startup() {
        state = TimeState.Running;
    }

    void Update() {
        if (state == TimeState.Running) {
            timeLeft -= Time.deltaTime;

            if(timeLeft <= 0f) {
                TriggerOutOfTimeEvent();
            }
        }
    }

    /// <summary>
    /// Triggers the event where the time runs out
    /// </summary>
    void TriggerOutOfTimeEvent() {
        state = TimeState.Stopped;
        if (OutOfTimeEvent != null) {
            OutOfTimeEvent();
        }
    }

}
