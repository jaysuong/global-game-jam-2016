using UnityEngine;
using System.Collections;

public class OutOfTimeTasks : MonoBehaviour {

    GameManager gameManager;
    TimeManager timeManager;

    public GameObject loseScreen;
    public GameObject hudScreen;


    void Start() {
        gameManager = FindObjectOfType<GameManager>();
        timeManager = FindObjectOfType<TimeManager>();
    }

    void OnEnable() {
        if(timeManager == null) {
            timeManager = FindObjectOfType<TimeManager>();
        }
        timeManager.OutOfTimeEvent += EndGame;
    }

    void OnDisable() {
        timeManager.OutOfTimeEvent -= EndGame;
    }

	public void EndGame() {
        gameManager.SetState(GameManager.GameState.Complete);
        timeManager.enabled = false;
        loseScreen.SetActive(true);
        hudScreen.SetActive(false);
        FindObjectOfType<Hero>().SwitchToDefeat();
    }
}
