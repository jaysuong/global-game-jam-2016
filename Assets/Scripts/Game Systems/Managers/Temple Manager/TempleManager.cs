using UnityEngine;
using System.Collections;
using System;

public class TempleManager : MonoBehaviour, IStartable {

    public TemplePiece[] pieces;

    public int piecesToBeCompleted = 3;

    TemplePiece currentPiece;
    int index;

    GameManager manager;


    void Start() {
        index = 0;
        manager = FindObjectOfType<GameManager>();
    }

    public void Startup() {
        currentPiece = Instantiate(pieces[index]);
    }

    void Update() {
        if(manager.state == GameManager.GameState.Running) {
            if(index < piecesToBeCompleted) {
                if(currentPiece.IsComplete) {
                    AdvancePiece();
                }
            }
        }
    }

    void AdvancePiece() {
        currentPiece.Cleanup();
        Destroy(currentPiece.gameObject);
        int next = index + 1;

        if (next < piecesToBeCompleted) {
            currentPiece = Instantiate(pieces[next]);
            index = next;
        }
        else {
            FindObjectOfType<WinGameTasks>().EndGame();
        }
    }

}
