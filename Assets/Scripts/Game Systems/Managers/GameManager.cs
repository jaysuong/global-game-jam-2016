using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour {

    public enum GameState { Starting, Running, Paused, Complete }



    public bool startAutomatically = true;

    public GameState state { get; private set; }


    void Start() {
        if (startAutomatically) {
            RunStartables();
        }
        else {
            state = GameState.Starting;
        }
    }


    /// <summary>
    /// Runs all scripts with IStartable interface
    /// </summary>
    public void RunStartables() {
        IStartable[] startables = gameObject.GetInterfacesOfType<IStartable>();
        for (int i = 0; i < startables.Length; ++i) {
            startables[i].Startup();
        }
        state = GameState.Running;
    }

    public void SetState(GameState state) {
        this.state = state;
    }
}
