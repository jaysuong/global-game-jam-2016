using UnityEngine;
using System.Collections;

public class WinGameTasks : MonoBehaviour {

    GameManager gameManager;
    TimeManager timeManager;

    public GameObject winScreen;
    public GameObject hudScreen;


    void Start() {
        gameManager = FindObjectOfType<GameManager>();
        timeManager = FindObjectOfType<TimeManager>();
    }

    public void EndGame() {
        gameManager.SetState(GameManager.GameState.Complete);
        timeManager.enabled = false;
        winScreen.SetActive(true);
        hudScreen.SetActive(false);
        FindObjectOfType<Hero>().SwitchToVictory();
    }
}
