using UnityEngine;
using System.Collections;

/// <summary>
/// Interface for manual starts for MonoBehaviours
/// </summary>
public interface IStartable {

    void Startup();
}
