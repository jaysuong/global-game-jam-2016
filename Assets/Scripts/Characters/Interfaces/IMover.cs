using UnityEngine;
using System.Collections;

public interface IMover {

    void Move();
}
