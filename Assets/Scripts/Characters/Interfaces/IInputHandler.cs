using UnityEngine;
using System.Collections;


/// <summary>
/// Interface for handling player inputs
/// </summary>
public interface IInputHandler {

    void MoveTowards(Vector2 position);
}
