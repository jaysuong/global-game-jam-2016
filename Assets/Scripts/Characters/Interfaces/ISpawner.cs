using UnityEngine;
using System.Collections;

/// <summary>
/// Interface for handling spawns
/// </summary>
public interface ISpawner {

    void Spawn(Vector2 throwVector);
    
}
