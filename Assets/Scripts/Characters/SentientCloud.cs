using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SentientCloud : MonoBehaviour, IMover, ISpawner {

    public float spawnInterval = 1.5f;

    public float cloudWidth = 3f;

    public float width = 2f;

    public float frequency = 1f;

    public Sprite[] sprites;

    public SpriteRenderer spriteRenderer;


    Vector3 original;

    // Dummy Vector2
    Vector2 dummyVector;

    float spawnTimer;
    
    ItemManager inventory;
    GameManager manager;

    /// <summary>
    /// The name of the blocks from the inventory
    /// </summary>
    string[] blockNames;


    void Start() {
        dummyVector = new Vector2(0, 0);
        spawnTimer = spawnInterval;
        inventory = FindObjectOfType<ItemManager>();
        BuildBlockNames();
        manager = FindObjectOfType<GameManager>();
        original = transform.position;
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
    }

    void Update() {

        if (manager.state != GameManager.GameState.Running)
            return;

        transform.position = new Vector3(original.x + Mathf.Sin(Time.time * frequency) * width,
            transform.position.y, 0);

        spawnTimer -= Time.deltaTime;
        if(spawnTimer <= 0) {
            Spawn(dummyVector);
            spawnTimer = spawnInterval;
        }
    }


    public void Move() {
        
    }

    public void Spawn(Vector2 ThrowVector) {
        Item item = inventory.GetItem(blockNames[Random.Range(0, blockNames.Length)]);
        if(item != null) {
            Vector3 spawn = new Vector3(transform.position.x + Random.Range(-cloudWidth, cloudWidth),
                transform.position.y, transform.position.z);
            item.transform.position = spawn;
            item.GetComponent<Rigidbody2D>().velocity = Vector2.up * Physics2D.gravity.y;
        }
    }


    #region Setups

    void BuildBlockNames() {
        blockNames = new string[inventory.inventory.Length];
        for (int i = 0; i < blockNames.Length; ++i) {
            blockNames[i] = inventory.inventory[i].name;
        }
    }

    #endregion

}
