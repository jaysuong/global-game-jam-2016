using UnityEngine;
using System.Collections;

public class HeroInput : MonoBehaviour {
    public float valueDifference = 2f;

    private Vector3 targetPosition;

    private IInputHandler iHandler;

    private ISpawner iSpawn;

    private Vector3 pressedPosition;
    private Vector3 releasePosition;

    // Use this for initialization
    void Start () {
        iHandler = gameObject.GetInterface<IInputHandler>();
        iSpawn = gameObject.GetInterface<ISpawner>();
	}
	
	// Update is called once per frame
	void Update () {
        // Test by keyboard (This can be switched out with touch
        
        if (Input.GetKey(KeyCode.Mouse0)) {
            RaycastHit hit;
            Ray ray;

#if UNITY_EDITOR
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //for touch device
#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
           ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
#endif

            if(Physics.Raycast(ray, out hit)) {
                targetPosition.x = hit.point.x;
            }
            iHandler.MoveTowards(targetPosition);
        }
        ThrowBlockUp();
    }

    /// <summary>
    /// Checks if the player is swiping up
    /// </summary>
    public bool CanThrowBlockUp() {
        RaycastHit hit;
        Ray ray;

        if (Input.GetKeyDown(KeyCode.Mouse0)) {

#if UNITY_EDITOR
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //for touch device
#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
           ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
#endif
            if (Physics.Raycast(ray, out hit)) {
                pressedPosition = hit.point;
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0)){

#if UNITY_EDITOR
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //for touch device
#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
           ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
#endif
            if (Physics.Raycast(ray, out hit)) {
                releasePosition = hit.point;

                if ((releasePosition.y - pressedPosition.y) > 0 && 
                    (Vector3.Distance(pressedPosition, releasePosition) > 
                    valueDifference)) {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Cultist throws the block up if the player swipes up
    /// </summary>
    public void ThrowBlockUp() {
        if (CanThrowBlockUp() == true) {
            iSpawn.Spawn(CalcThrowAngle());
        }
    }

    public Vector2 CalcThrowAngle() {
        float yDifference = releasePosition.y - pressedPosition.y;
        float xDifference = releasePosition.x - pressedPosition.x;

        return new Vector2(xDifference, yDifference);
    }


}

