using UnityEngine;
using System.Collections.Generic;
using System;

public class Hero : MonoBehaviour, ISpawner, IInputHandler {

    public enum HeroState { Idle = 0, Run = 1, Catch = 2, Throw = 3, Victory = 4, Defeat = 5 }

    public float moveSpeed = 10;
    
    [Tooltip("How far must the hero travel to be considered as \"moving\"")]
    public float movementThreshold = 0.1f;

    public float throwSpeed = 10f;

    public Transform caughtPoint;

    [Tooltip("Order is: Idle, Run, Catch, Throw")]
    public SpriteList[] sprites;

    public float spriteInterval = 0.1f;


    public HeroState state { get; private set; }
    public Block currentBlock { get; private set; }

    bool isStarted = false;
    
    /// <summary>
    /// Is the current state locked?
    /// </summary>
    bool isStateLocked = false;

    Vector3 destination;
    Vector3 lastKnownPosition;

    SpriteRenderer spriteRenderer;
    int spriteIndex;
    float spriteTimer;

    GameManager manager;


    #region Class States

    [Serializable]
    public class SpriteList {
        public Sprite[] sprites;
    }

    #endregion


    #region Unity Messages

    void Start() {
        destination = transform.position;
        state = HeroState.Idle;
        spriteRenderer = GetComponent<SpriteRenderer>();
        lastKnownPosition = transform.position;
        spriteIndex = 0;
        spriteTimer = spriteInterval;
        manager = FindObjectOfType<GameManager>();
    }

    void Update() {

        if (manager.state != GameManager.GameState.Running)
            return;

        HandleMovement();
        
        //Handle sprites
        spriteTimer -= Time.deltaTime;

        if(spriteTimer <= 0) {
            spriteTimer = spriteInterval;
            spriteIndex = (spriteIndex + 1) % sprites[(int)state].sprites.Length;
        }
        spriteRenderer.sprite = sprites[(int)state].sprites[spriteIndex];
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(manager.state == GameManager.GameState.Running && other.CompareTag("Block")) {
            if(currentBlock == null) {
                currentBlock = other.GetComponent<Block>();

                if (currentBlock != null) {
                    currentBlock.HandleCaught(transform);
                    currentBlock.transform.localPosition = caughtPoint.localPosition;
                    SwitchToCatch();
                }
            }
        }
    }

    #endregion

    public void MoveTowards(Vector2 position) {
        if (manager.state != GameManager.GameState.Running)
            return;

        if (state == HeroState.Idle || state == HeroState.Run)
            destination = new Vector3(position.x, transform.position.y, 0);
    }

    public void Spawn(Vector2 throwVector) {
        if (manager.state != GameManager.GameState.Running)
            return;

        if(currentBlock != null) {
            SwitchToThrow();
            currentBlock.HandleThrowing(throwSpeed, throwVector);
            currentBlock = null;
        }
    }

    #region States

    public void SwitchState(HeroState state) {
        if (isStateLocked == false) {
            this.state = state;
            spriteIndex = 0;
            spriteTimer = spriteInterval;
        }
    }

    bool IsCatchingOrThrowing() {
        return (state == HeroState.Catch || state == HeroState.Throw) && isStateLocked;
    }

    public void SwitchToThrow() {
        CancelInvoke();
        ResetLock();
        SwitchState(HeroState.Throw);
        isStateLocked = true;
        Invoke("ResetLock", 0.5f);
    }

    public void SwitchToCatch() {
        CancelInvoke();
        ResetLock();
        SwitchState(HeroState.Catch);
        isStateLocked = true;
    }

    public void SwitchToVictory() {
        CancelInvoke();
        ResetLock();
        SwitchState(HeroState.Victory);
        isStateLocked = true;
        spriteRenderer.sprite = sprites[(int)HeroState.Victory].sprites[0];
        if (currentBlock != null) {
            currentBlock.Despawn();
        }
    }

    public void SwitchToDefeat() {
        CancelInvoke();
        ResetLock();
        SwitchState(HeroState.Defeat);
        isStateLocked = true;
        spriteRenderer.sprite = sprites[(int)HeroState.Defeat].sprites[0];
        if(currentBlock != null) {
            currentBlock.Despawn();
        }
    }

    public void ResetLock() {
        CancelInvoke();
        isStateLocked = false;
    }

    #endregion

    #region Movements

    public void HandleMovement() {
        transform.position = Vector3.Lerp(transform.position, destination,
            Time.deltaTime * moveSpeed);

        Vector3 current = transform.position;

        if (current != lastKnownPosition) {
            if (Mathf.Abs(current.x - lastKnownPosition.x) > movementThreshold) {

                if (state != HeroState.Run && !IsCatchingOrThrowing()) {
                    SwitchState(HeroState.Run);
                }

                //spriteRenderer.flipX = current.x < lastKnownPosition.x;
                if(current.x < lastKnownPosition.x) {
                    transform.localEulerAngles = new Vector3(0, 180, 0);
                }
                else {
                    transform.localEulerAngles = Vector3.zero;
                }
            }
            else if(!IsCatchingOrThrowing()) {
                SwitchState(HeroState.Idle);
            }

            lastKnownPosition = transform.position;
        }
        else {
            if (state != HeroState.Idle) {
                SwitchState(HeroState.Idle);
            }
        }
    }


    #endregion
}
