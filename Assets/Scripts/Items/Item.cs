using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    public float activeTime = 20f;


	public virtual void Spawn() {
        gameObject.SetActive(true);
        Invoke("Despawn", activeTime);
    }

    public virtual void Despawn() {
        CancelInvoke();
        gameObject.SetActive(false);
    }
}
