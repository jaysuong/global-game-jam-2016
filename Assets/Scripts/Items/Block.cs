using UnityEngine;
using System.Collections;

public enum BlockState { Idle, Falling, Caught, Thrown }

public class Block : Item {

    public GameObject particlePrefab;


    public BlockState state { get; protected set; }


    void Start() {
        state = BlockState.Falling;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Block")) {
            Block block = other.GetComponent<Block>();
            if(block != null && block.state == BlockState.Thrown) {
                //Cause explosion
                GameObject obj = Instantiate(particlePrefab);
                obj.transform.position = transform.position;

                block.Despawn();
                Despawn();
            }
        }
        else if (other.CompareTag("Wall")) {
            Rigidbody2D block = GetComponent<Rigidbody2D>();
            float xVelocity = block.velocity.x;
            float yVelocity = block.velocity.y;
            GetComponent<Rigidbody2D>().velocity = new Vector2(-xVelocity,
                yVelocity);
        }
    }

    public override void Spawn() {
        state = BlockState.Falling;
        GetComponent<Collider2D>().enabled = true;
        base.Spawn();
    }

    public override void Despawn() {
        base.Despawn();
        transform.SetParent(null);
        GetComponent<Collider2D>().enabled = false;
        state = BlockState.Idle;
    }


    public void HandleCaught(Transform transform) {
        state = BlockState.Caught;
        CancelInvoke();
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        this.transform.SetParent(transform);
    }

    public void HandleThrowing(float throwSpeed) {
        state = BlockState.Thrown;
        transform.SetParent(null);
        GetComponent<Rigidbody2D>().velocity = Vector2.up * throwSpeed;
        Invoke("Despawn", 10f);
    }

    public void HandleThrowing(float throwSpeed, Vector2 throwVector) {
        state = BlockState.Thrown;
        transform.SetParent(null);
        GetComponent<Rigidbody2D>().velocity = throwVector * throwSpeed;
        Invoke("Despawn", 10f);
    }
       
}
