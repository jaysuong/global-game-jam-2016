using UnityEngine;
using System.Collections.Generic;

public class ItemManager : MonoBehaviour {

    [HideInInspector]
    public ItemInfo[] inventory;

    public ItemInfo[] templePieces;


    #region Class Definitions

    [System.Serializable]
    public class ItemInfo {
        public string name;
        public Item prefab;
        public int amount = 10;
    }

    #endregion

    Dictionary<string, List<Item>> pool;
    Dictionary<string, ItemInfo> slotPool; 


    void Awake() {
        pool = new Dictionary<string, List<Item>>();
        slotPool = new Dictionary<string, ItemInfo>();
        SetupSlots();
        PoolItems();
    }

    #region Setup

    void SetupSlots() {
        for(int i = 0; i < templePieces.Length; ++i) {
            slotPool.Add(templePieces[i].name, templePieces[i]);
        }
    }

    void PoolItems() {
        for(int i = 0; i < inventory.Length; ++i) {
            List<Item> items = new List<Item>(inventory[i].amount);
            for(int k = 0; k < inventory[i].amount; ++k) {
                Item item = Instantiate(inventory[i].prefab);
                item.gameObject.SetActive(false);
                item.name = inventory[i].prefab.name;
                items.Add(item);
            }
            pool.Add(inventory[i].name, items);
        }
    }

    #endregion

    #region Getters

    public Item GetItem(string name) {
        for(int i = 0; i < pool[name].Count; ++i) {
            Item candidate = pool[name][i];
            if(!candidate.gameObject.activeInHierarchy) {
                pool[name][i].Spawn();
                return pool[name][i];
            }
        }
        return null;
    }

    public Item GetSlot(string name) {
        Item slot = Instantiate(slotPool[name].prefab);
        return slot;
    }

    #endregion

}
