using UnityEngine;
using System.Collections;

public class TemplePiece : MonoBehaviour {

    public TempleSlot[] slots;

    ItemManager inventory;

    public bool IsComplete { get
        {
            for(int i = 0; i < slots.Length; ++i) {
                if (!slots[i].isMatched)
                    return false;
            }
            return true;
        }
    }


    public void Start() {
        inventory = FindObjectOfType<ItemManager>();
        RandomizeSlots();
    }

    /// <summary>
    /// Randomizes slots
    /// </summary>
    public void RandomizeSlots() {
        ItemManager.ItemInfo[] infos = inventory.inventory;

        for(int i = 0; i < slots.Length; ++i) {
            slots[i].matchName = infos[Random.Range(0, infos.Length)].name;
            Item slotItem = inventory.GetSlot(slots[i].matchName);
            slotItem.transform.position = slots[i].transform.position;
            slotItem.transform.SetParent(slots[i].transform);
        }
    }

    public void Cleanup() {
        for(int i = 0; i < slots.Length; ++i) {
            slots[i].Cleanup();
        }
    }
    
}
