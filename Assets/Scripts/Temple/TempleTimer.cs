using UnityEngine;
using System.Collections;

public class TempleTimer : MonoBehaviour {

    public Color duskColor;

    public Color nightColor = Color.black;

    public float duskTime = 60f;

    float nightTime;

    public SpriteRenderer spriteRenderer;
    Color startColor;

    GameManager manager;
    TimeManager timeManager;



    void Start() {
        manager = FindObjectOfType<GameManager>();
        timeManager = FindObjectOfType<TimeManager>();
        startColor = spriteRenderer.color;
        nightTime = timeManager.gameLength - duskTime;
    }


    // Update is called once per frame
    void Update() {
        //if it's not dusk yet

        if(manager.state != GameManager.GameState.Running) {
            return;
        }

        float elapsedTime = timeManager.elaspedTime;


        if (elapsedTime < duskTime) {
            spriteRenderer.color = Color.Lerp(startColor, duskColor,
                elapsedTime / duskTime);
        }
        else {
            spriteRenderer.color = Color.Lerp(spriteRenderer.color, nightColor,
                Time.deltaTime * (1f / nightTime));
        }
    }
}
