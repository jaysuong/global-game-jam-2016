using UnityEngine;
using System.Collections;

public class TempleSlot : MonoBehaviour {

    public string matchName;

    public bool isMatched { get {
            return caughtBlock != null && caughtBlock.transform.parent == transform;
        } }

    public Block caughtBlock { get; private set; }



	void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Block")) {
            Block block = other.GetComponent<Block>();

            if(block != null && block.state == BlockState.Thrown && 
                block.name == matchName) {

                block.HandleCaught(transform);
                block.transform.position = transform.position;
                caughtBlock = block;
            }
        }
    }

    public void Cleanup() {
        if(caughtBlock != null) {
            caughtBlock.Despawn();
        }
    }

}
