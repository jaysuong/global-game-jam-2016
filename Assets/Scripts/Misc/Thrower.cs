using UnityEngine;
using System.Collections;

public class Thrower : MonoBehaviour {

    ISpawner spawner;

    Vector2 dummyVector;

	// Use this for initialization
	void Start () {
        spawner = gameObject.GetInterface<ISpawner>();
        dummyVector = new Vector2(0, 0);
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Input.GetKeyUp(KeyCode.Space)) {
            spawner.Spawn(dummyVector);
        }
	}
}
