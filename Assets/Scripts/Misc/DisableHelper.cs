using UnityEngine;
using System.Collections;

public class DisableHelper : MonoBehaviour {


    public void Enable() {
        gameObject.SetActive(true);
    }

	public void Disable() {
        gameObject.SetActive(false);
    }
}
