using UnityEngine;
using System.Collections;

public class JitterSprite : MonoBehaviour {
    public float interval;
    float timer;
    public float maxoffset = .1f;
    Vector3 originalposition;

	// Use this for initialization
	void Start () {
        timer = interval;
        originalposition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        timer = timer - Time.deltaTime;
        if (timer <= 0) {
            timer = interval;
            Vector3 offset = new Vector3(Random.Range(-maxoffset,maxoffset), (Random.Range(-maxoffset, maxoffset)));
            transform.localPosition = originalposition  + offset;
        }
	}
}
