using UnityEngine;
using System.Collections;

public class Autokill : MonoBehaviour {

    public float liveTime = 3f;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, liveTime);
	}
	
}
