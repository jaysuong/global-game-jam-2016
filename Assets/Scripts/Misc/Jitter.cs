using UnityEngine;
using System.Collections;

public class Jitter : MonoBehaviour {

    public float jitterAmount = 5f;

    public float interval = 0.1f;

    Vector3 startingPos;

	// Use this for initialization
	void Start () {
        startingPos = transform.localPosition;
        StartCoroutine(JitterSprite());
	}
	
    IEnumerator JitterSprite() {
        while(true) {
            transform.localPosition = startingPos + new Vector3(
                Random.Range(-jitterAmount, jitterAmount), 
                Random.Range(-jitterAmount, jitterAmount), 
                0f);
            yield return new WaitForSeconds(interval);
        }  
    }
	
}
