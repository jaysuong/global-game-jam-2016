using UnityEngine;
using System.Collections;

public class SizePulser : MonoBehaviour {

    public Vector3 minSize;

    public Vector3 maxSize;

    public float frequency;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale = Vector3.Lerp(minSize, maxSize, Mathf.Abs(Mathf.Sin(Time.time * frequency)));
	}
}
