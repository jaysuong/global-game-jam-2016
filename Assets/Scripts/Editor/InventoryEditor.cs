using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(ItemManager))]
public class InventoryEditor : Editor {

    ItemManager manager;

    ReorderableList itemList;
    //ReorderableList slotList;


    void OnEnable() {
        manager = (ItemManager)target;
        itemList = new ReorderableList(manager.inventory, typeof(Item));
        itemList.drawHeaderCallback += DrawInventoryHeader;
        itemList.drawElementCallback += DrawElement;

        //slotList = new ReorderableList(manager.templePieces, typeof(Item), 
        //    true, true, true, true);
        //slotList.drawHeaderCallback += DrawSlotHeader;
        //slotList.drawElementCallback += DrawSlotElement;
        //slotList.onAddCallback += AddSlotElement;
        //slotList.onRemoveCallback += RemoveSlotElement;        
    }

    void OnDisable() {
        itemList.drawHeaderCallback -= DrawInventoryHeader;
        itemList.drawElementCallback -= DrawElement;

        //slotList.drawHeaderCallback -= DrawSlotHeader;
        //slotList.drawElementCallback -= DrawSlotElement;
        //slotList.onAddCallback -= AddSlotElement;
        //slotList.onRemoveCallback -= RemoveSlotElement;
    }

    public override void OnInspectorGUI() {
        EditorGUILayout.Space();
        itemList.DoLayoutList();
        EditorGUILayout.Space();
        base.OnInspectorGUI();
        //EditorGUILayout.Space();
        //slotList.DoLayoutList();
    }

    #region Draw Methods

    void DrawInventoryHeader(Rect rect) {
        GUI.Label(rect, new GUIContent("Inventory"));
    }

    void DrawElement(Rect rect, int index, bool active, bool focused) {
        ItemManager.ItemInfo info = manager.inventory[index];
        float height = EditorGUIUtility.singleLineHeight;
        float singleWidth = rect.width / 6f;
        float offset = rect.y + 3f;

        info.name = GUI.TextField(new Rect(rect.x, offset, singleWidth * 2.5f, height),
            info.name);
        info.prefab = (Item)EditorGUI.ObjectField(
            new Rect(rect.x + singleWidth * 2.5f, offset, singleWidth * 2.5f, height), 
            GUIContent.none, info.prefab, 
            typeof(Item), false);
        info.amount = EditorGUI.IntField(
            new Rect(rect.x + rect.width - singleWidth, offset, singleWidth, height), 
            info.amount);
    }

    #endregion

    //#region Slot Draws

    //void DrawSlotHeader(Rect rect) {
    //    GUI.Label(rect, new GUIContent("Temple Slots"));
    //}

    //void DrawSlotElement(Rect rect, int index, bool active, bool focused) {
    //    ItemManager.ItemInfo info = manager.templePieces[index];
    //    float height = EditorGUIUtility.singleLineHeight;
    //    float singleWidth = rect.width / 6f;
    //    float offset = rect.y + 3f;

    //    info.name = GUI.TextField(new Rect(rect.x, offset, singleWidth * 2.5f, height),
    //        info.name);
    //    info.prefab = (Item)EditorGUI.ObjectField(
    //        new Rect(rect.x + singleWidth * 2.5f, offset, singleWidth * 2.5f, height),
    //        GUIContent.none, info.prefab,
    //        typeof(Item), false);
    //    info.amount = EditorGUI.IntField(
    //        new Rect(rect.x + rect.width - singleWidth, offset, singleWidth, height),
    //        info.amount);
    //}

    //void AddSlotElement(ReorderableList list) {

    //}

    //void RemoveSlotElement(ReorderableList list) {

    //}

    //#endregion


}
