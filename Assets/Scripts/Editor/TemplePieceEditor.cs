using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TemplePiece))]
public class TemplePieceEditor : Editor {

    TemplePiece piece;

    void OnEnable() {
        piece = (TemplePiece)target;
    }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        if (GUILayout.Button(new GUIContent("Fetch From Children"), EditorStyles.miniButton)) {
            TempleSlot[] slots = piece.GetComponentsInChildren<TempleSlot>(true);
            piece.slots = slots;
            EditorUtility.SetDirty(piece);
        }
    }

}
